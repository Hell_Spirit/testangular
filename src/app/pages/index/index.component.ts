import { Component, OnInit } from '@angular/core';
import { ChartsService } from '../charts/components/echarts/charts.service';
import { StubService } from '../../shared/services/stubService';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [ChartsService,StubService]
})
export class IndexComponent implements OnInit {
  showloading: boolean = false;
  
  public AnimationBarOption;
  userDetail: any;
  pageSize = 7;
  pageNumber = 1;
  pdfSrc: string;
  constructor(private _chartsService: ChartsService,private stubService:StubService) { }

  ngOnInit() {
    this.AnimationBarOption = this._chartsService.getAnimationBarOption();
    this.userDetails();
  }



  userDetails(){
this.stubService.getjson().subscribe(data => {
  this.userDetail=data
});
  }

  resumeOpen(data) {
    this.pdfSrc=data;
    document.getElementById("mySidenav").style.width = "58%";
  }
  
   closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  pageChanged(pN: number): void {
    this.pageNumber = pN;
  }
}
