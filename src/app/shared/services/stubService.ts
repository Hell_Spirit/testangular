import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Injectable} from '@angular/core';
@Injectable()
export class StubService{
  constructor(private http: HttpClient) {
  }
  public getjson(): Observable<any> {
    return this.http.get('assets/constant/userName.json');
  }
 
  
}
